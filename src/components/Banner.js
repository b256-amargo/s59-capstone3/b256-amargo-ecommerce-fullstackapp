
import { Link } from "react-router-dom";
import { Button, Row, Col } from "react-bootstrap";

export default function Banner({data}){

	const {title, content, destinationOne, destinationTwo, labelOne, labelTwo} = data;

	return(
		<Row>
			<Col className="p-5 text-center text-light">
				<h1 className="mt-3">{title}</h1>
				<p className="mb-3">{content}</p>
				<Row className="d-flex justify-content-around m-5 p-2">
					<Button className="w-50 m-3 p-2 border border-light text-light" as={Link} to={destinationOne} variant="transparent">{labelOne}</Button>
					<Button className="w-50 m-3 p-2 border border-light text-light" as={Link} to={destinationTwo} variant="transparent">{labelTwo}</Button>
				</Row>
			</Col>
		</Row>
	)
}