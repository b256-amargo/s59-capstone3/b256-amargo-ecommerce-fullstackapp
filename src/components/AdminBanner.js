
import { Link } from "react-router-dom";
import { Button, Row, Col } from "react-bootstrap";

export default function Banner({data}){

	const {title, content} = data;

	return(
		<Row>
			<Col className="p-5 text-center text-light">
				<h1>{title}</h1>
				<p>{content}</p>
				<Row className="d-flex justify-content-around m-3 p-2">
					<Button className="w-50 m-3 p-2 border border-light text-light" as={Link} to={"/createProduct"} variant="transparent">
						Create New Product
					</Button>
					<Button className="w-50 m-3 p-2 border border-light text-light" as={Link} to={"/allProducts"} variant="transparent">
						View All Products
					</Button>
				</Row>
			</Col>
		</Row>
	)
}