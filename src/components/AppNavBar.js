
import { Container, Navbar, Nav, Image } from "react-bootstrap"
import { Link, NavLink } from "react-router-dom";
import { useContext } from "react";
import UserContext from "../userContext";
import image from "../images/OriginiumIngot.png";

export default function AppNavBar(){

	const { user } = useContext(UserContext);

	return(
		<Navbar expand="lg">
      		<Container>
		        <Navbar.Brand as={Link} to="/"><Image src={image} width="30"></Image></Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          	<Nav className="me-auto">
		            	<Nav.Link className="text-center text-light" as={NavLink} to="/">Home</Nav.Link>
		            	{
		            		(user.isAdmin) ?
		            			<>
		            			<Nav.Link className="text-center text-light" as={NavLink} to="/adminDashboard">Dashboard</Nav.Link>
		            			<Nav.Link className="text-center text-light" as={NavLink} to="/products" hidden>Products</Nav.Link>
		            			</>
		            		:
		            			<>
		            			<Nav.Link className="text-center text-light" as={NavLink} to="/adminDashboard" hidden>Dashboard</Nav.Link>
		            			<Nav.Link className="text-center text-light" as={NavLink} to="/products">Products</Nav.Link>
		            			</>
		            	}

		            	{
		            		(user.id !== null) ?
		            			<Nav.Link className="text-center text-light" as={NavLink} to="/logout">Logout</Nav.Link>
		            		:
		            			<>
		            			<Nav.Link className="text-center text-light" as={NavLink} to="/login">Login</Nav.Link>
		            			<Nav.Link className="text-center text-light" as={NavLink} to="/register">Register</Nav.Link>
		            			</>
		            	}
		          	</Nav>
		        </Navbar.Collapse>
      		</Container>
    	</Navbar>
	)
}