
import { Card, Button } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import { React, useContext } from "react";
import UserContext from "../userContext";
import Swal from "sweetalert2";

export default function ProductCard({productProp}){

	const { name, description, price, _id, isActive } = productProp;
	const { user } = useContext(UserContext);
	const navigate = useNavigate();

    // ---------------

	function activateProduct(){

        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/activate`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(response => {
            console.log(response)
            if(response){
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Product is now enabled"
                })
                navigate("/allProducts")
            }
            else{
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Something went wrong. Please try again"
                })
                navigate("/allProducts")
            }
        })
    }

    // ---------------

    function archiveProduct(){

        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(response => {
            console.log(response)
            if(response){
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Product is now disabled"
                })
                navigate("/allProducts")
            }
            else{
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Something went wrong. Please try again"
                })
                navigate("/allProducts")
            }
        })
    }

    // ---------------

	return(
        <Card style={{ width: '100%' }} className="d-flex flex-row bg-secondary text-light border border-light p-2 ml-5 mt-5 mb-5 justify-content-around bg-opacity-50">
            <Card.Body>

                <Card.Title className="mb-4">{name}</Card.Title>

                {
                    (!user.isAdmin) ?
                        <>
                        <Card.Subtitle hidden>Description:</Card.Subtitle>
                        <Card.Text hidden>
                          {description}
                        </Card.Text>
                        </>
                    :
                        <>
                        <Card.Subtitle className="mb-1 mt-2">Description:</Card.Subtitle>
                        <Card.Text className="mb-3">
                          {description}
                        </Card.Text>
                        </>
                }

                

                <Card.Subtitle className="mb-1 mt-2">Price:</Card.Subtitle>
                <Card.Text className="mb-3">
                  {price}
                </Card.Text>
                
                {
                    (!user.isAdmin) ?
                        <Button variant="success" as={Link} to={`/productView/${_id}`}>Details</Button>
                    :
                        <>
                        <Link className="btn btn-success btn-block m-2" to={`/editProduct/${_id}`}>Edit</Link>

                        {
                            (isActive) ?
                                <Link className="btn btn-danger btn-block m-2" onClick={() => archiveProduct()}>Archive</Link>
                            :
                                <Link className="btn btn-primary btn-block m-2" onClick={() => activateProduct()}>Activate</Link>
                        }
                        </>
                }
            </Card.Body>
        </Card>
	)
}