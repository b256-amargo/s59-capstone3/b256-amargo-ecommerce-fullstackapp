
import { useState, useEffect, useContext } from 'react';
import { Container, Card, Row, Col } from 'react-bootstrap';
import { useParams, Link } from "react-router-dom";
import UserContext from "../userContext";
// import image from "../images/Stelle.jpg";

export default function ProductView() {
	const { productId } = useParams();
	const { user } = useContext(UserContext);
	console.log(user);
	// const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	// const [imageLink, setImageLink] = useState("");

	// ---------------

	useEffect(() => {
		// console.log(productId);
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(response => response.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			// setImageLink(data.imageLink);
		})
	});

	// ---------------

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 4 }} className="container-fluid">
					<img variant="top" src={require('../images/Stelle.jpg')} alt={require('../images/Stelle.jpg')} classname="img-fluid" width="75%"/>
				</Col>
				<Col lg={{ span: 8}}>
					<Card className="text-center text-light bg-transparent border border-light p-3">
						<Card.Body>
							<Card.Title className="mb-4">{name}</Card.Title>
							<Card.Subtitle className="mb-2">Description:</Card.Subtitle>
							<Card.Text className="mb-4">{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{price}</Card.Text>

							{
								(user.id !== null) ?
								<Link className="btn btn-primary btn-block" to={`/productView/${productId}/checkout`}>Proceed to Checkout</Link>
								:
								<Link className="btn btn-danger btn-block" to="/login">Login</Link>
							}
							
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
