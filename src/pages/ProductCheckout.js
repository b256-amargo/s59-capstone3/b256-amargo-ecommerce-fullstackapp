
import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Navigate, Link } from "react-router-dom";
import UserContext from "../userContext";
import Swal from "sweetalert2";

export default function ProductCheckout() {
	const { productId } = useParams();
	const { user } = useContext(UserContext);
	console.log(user);
	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [orderNum, setOrderNum] = useState(0);

	// ---------------

	useEffect(() => {
		// console.log(productId);
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(response => response.json())
		.then(data => {
			// console.log(data)
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	});

	// ---------------

	const order = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify([
				{
					productId: productId,
					quantity: orderNum
				}
			])
		})
		.then(response => response.json())
		.then(data => {
			// console.log(data);
			if(data ===  true){
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "Order Successful"
				})
				navigate("/products")
			}
			else{
				Swal.fire({
					title: "Error!",
					icon: "error",
					text: "Something went wrong! Please try again"
				})
			}
		})
	};

	// ---------------

	return(
		(user.isAdmin) ?
            <Navigate to="/adminDashboard" />
        :
			<Container className="mt-5">
				<Row>
					<Col lg={{ span: 6, offset: 3 }}>
						<Card className="text-center text-light bg-transparent border border-light">
							<Card.Body>
								<Card.Title>{name}</Card.Title>
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{description}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>PhP {price}</Card.Text>

								<Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
						        	<Form.Label className="inline">
						        		Number of Order/s:
						        		<Form.Control type="number" placeholder="Enter a number" value={orderNum} onChange={e => setOrderNum(e.target.value)}/>
						        	</Form.Label>
						      	</Form.Group>

								{
									(user.id !== null) ?
									<Button className="btn btn-primary btn-block" onClick={() => order(productId)}>Order</Button>
									:
									<Link className="btn btn-danger btn-block" to="/login">Login</Link>
								}
								
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
	)
}
