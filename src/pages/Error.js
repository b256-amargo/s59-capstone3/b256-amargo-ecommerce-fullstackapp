
import { Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useContext } from 'react';
import UserContext from "../userContext";

export default function Error(){
	const { user } = useContext(UserContext);
	console.log(user);

	return(
		<Row>
			<Col className="p-5 text-center">
				<h1 className="text-light">404 - Not Found</h1>
				<p className="text-light">The page you are looking for cannot be found.</p>
				<Row>
					{
						(user.isAdmin) ?
						<Button as={Link} to={"/adminDashboard"} variant="primary">Back to Dashboard</Button>
						:
						<Button as={Link} to={"/products"} variant="primary">Back to Products</Button>
					}
				</Row>
			</Col>
		</Row>
	)
}