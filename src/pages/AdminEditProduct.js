
import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { useParams, Navigate, useNavigate } from "react-router-dom";
import UserContext from "../userContext";
import Swal from "sweetalert2";

export default function EditProduct(){

    const { user } = useContext(UserContext);
    console.log(user);
    const { productId } = useParams();
    const navigate = useNavigate();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");

    // ---------------

    const[isActive, setIsActive] = useState(false);
    
    const editProductForm = [name, description, price];
    let checkProdForm = editProductForm.every(function(detail) {
        return (detail);
    });

    useEffect (() => {
        if(checkProdForm === true){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    });

    // ---------------

    function editProduct(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(response => {
            console.log(response)
            if(response){
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Product Updated"
                })
                navigate("/adminDashboard")
            }
            else{
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Something went wrong. Please try again"
                })
                navigate("/allProducts")
            }
        })

        setName("");
        setDescription("");
        setPrice("");
    }

    // ---------------

    return(
        (!user.isAdmin) ?
            <Navigate to="/" />
        :
        <Form onSubmit={e => editProduct(e)}>

            <h2 className="mt-3 mb-3 text-light">Edit Product Information</h2>

            <Form.Group className="mt-3 mb-3" controlId="formBasicName">
                <Form.Label className="text-light">Name</Form.Label>
                <Form.Control className="w-50" size="sm" type="string" placeholder="Enter old/new product name" value={name} onChange={e => setName(e.target.value)}/>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicDescription">
                <Form.Label className="text-light">Description</Form.Label>
                <Form.Control className="w-50" size="sm" type="string" placeholder="Enter old/new product description" value={description} onChange={e => setDescription(e.target.value)}/>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPrice">
                <Form.Label className="text-light">Price</Form.Label>
                <Form.Control className="w-50" size="sm" type="string" placeholder="Enter old/new product price" value={price} onChange={e => setPrice(e.target.value)}/>
            </Form.Group>

            {
                isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                    </Button>
                :
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                    </Button>
            }
        </Form>
    )
}