
import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import UserContext from "../userContext";
import Swal from "sweetalert2";

export default function Login(){

	const { user, setUser } = useContext(UserContext);
	console.log(user);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const navigate = useNavigate();

	// --------------------------------

	const [isActive, setIsActive] = useState(false);

	useEffect (() => {
		if(email !== "" && password !== "") {
			setIsActive(true);
		}
		else{
			setIsActive(false);
		};
	});

	// --------------------------------

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
			// console.log(user);

			Swal.fire({
				title: "Authentication Successful",
				icon: "success",
				text: "Welcome to the E-Commerce App (Temp)!"
			})

			if(data.isAdmin){
				navigate("/adminDashboard")
			}
			else{
				navigate("/products")
			}
		})
	};

	// --------------------------------

	function loginUser(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			// console.log(data)
			if(typeof data.access !== "undefined"){
				localStorage.setItem("token", data.access);
				retrieveUserDetails(data.access);
				// console.log(user)
			}
			else{
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again."
				})
			}
		});

		setEmail("");
		setPassword("");
	};

	// --------------------------------

	return(
		(user.id !== null) ?
			<Navigate to="/" />
		:
		<Form onSubmit={e => loginUser(e)}>

			<h1 className="mt-3 mb-3 text-light">Login</h1>
			
	      	<Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
	        	<Form.Label className="text-light">Email Address</Form.Label>
	        	<Form.Control className="w-25" size="sm" type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	      	</Form.Group>

	      	<Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
	        	<Form.Label className="text-light">Password</Form.Label>
	        	<Form.Control className="w-25" size="sm" type="password" placeholder="Enter password" value={password} onChange={e => setPassword(e.target.value)}/>
	      	</Form.Group>

	      	{
	      		isActive ?
	      			<Button variant="success" type="submit" id="submitBtn">
			        Login
			      	</Button>
			    :
	      			<Button variant="success" type="submit" id="submitBtn" disabled>
			        Login
			      	</Button>
	      	}

	        {/*<Form.Text className="text-muted mt-5">
	        	Don't have an account yet? <Button>Register here</Button>
	        </Form.Text>*/}

	    </Form>
	)
}