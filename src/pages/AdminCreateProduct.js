
import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import UserContext from "../userContext";
import Swal from "sweetalert2";

export default function CreateProduct(){

    const { user } = useContext(UserContext);
    console.log(user);
    const navigate = useNavigate();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");

    // ---------------

    const[isActive, setIsActive] = useState(false);
    
    const createProductForm = [name, description, price];
    let checkProdForm = createProductForm.every(function(detail) {
        return (detail);
    });

    useEffect (() => {
        if(checkProdForm === true){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    });

    // ---------------

    function createProduct(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(response => response.json())
        .then(data => {
            console.log(data)
            if(data){
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Product Added"
                })
                navigate("/adminDashboard")
            }
            else{
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Something went wrong. Please try again"
                })
                navigate("/createProduct")
            }
        })

        setName("");
        setDescription("");
        setPrice("");
    }

    // ---------------

    return(
        (!user.isAdmin) ?
            <Navigate to="/products" />
        :
        <Form onSubmit={e => createProduct(e)}>

            <h3 className="mt-3 mb-3 text-light">Add a New Product</h3>

            <Form.Group className="mt-3 mb-3" controlId="formBasicName">
                <Form.Label className="text-light">Name</Form.Label>
                <Form.Control className="w-50" size="sm" type="string" placeholder="Enter product name" value={name} onChange={e => setName(e.target.value)}/>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicDescription">
                <Form.Label className="text-light">Description</Form.Label>
                <Form.Control className="w-50" size="sm" type="string" placeholder="Enter product description" value={description} onChange={e => setDescription(e.target.value)}/>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPrice">
                <Form.Label className="text-light">Price</Form.Label>
                <Form.Control className="w-50" size="sm" type="string" placeholder="Enter product price" value={price} onChange={e => setPrice(e.target.value)}/>
            </Form.Group>

            {
                isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                    </Button>
                :
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                    </Button>
            }
        </Form>
    )
}