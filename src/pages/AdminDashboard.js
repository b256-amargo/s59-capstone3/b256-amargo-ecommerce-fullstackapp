
import AdminBanner from "../components/AdminBanner";
import { useContext } from "react";
import UserContext from "../userContext";
import { Navigate } from "react-router-dom";

export default function AdminDashboard(){

    const { user } = useContext(UserContext);
    console.log(user);

	const adminHomeData = {
        title: "Welcome, Boss!",
        content: "What do you want to do?",
    }

    return (
        (!user.isAdmin) ?
            <Navigate to="/products" />
        :
            <AdminBanner data={adminHomeData}/>
    )
}