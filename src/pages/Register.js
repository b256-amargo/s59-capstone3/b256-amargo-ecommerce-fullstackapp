
import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import UserContext from "../userContext";
import Swal from "sweetalert2";

export default function Register(){

	const { user } = useContext(UserContext);
	console.log(user);
	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	// ---------------

	const[isActive, setIsActive] = useState(false);
	
	const userRegisterForm = [name, email, password1, password2];
	let checkUserForm = userRegisterForm.every(function(detail) {
        return (detail);
    });

	useEffect (() => {
		if((checkUserForm === true) && (password1 === password2)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	});

	// ---------------

	function registerUser(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
			if(data){
				Swal.fire({
					title: "Email already exists!",
					icon: "error",
					text: "Please provide a different email"
				})
			}
			else{
				fetch("http://localhost:4000/users/register", {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						name: name,
						email: email,
						password: password1
					})
				})
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "You have successfully registered"
				})
				navigate("/login")
			}
		})

		setName("");
		setEmail("");
		setPassword1("");
		setPassword2("");
	}

	// ---------------

	return(
		(user.id !== null) ?
			<Navigate to="/" />
		:
		<Form onSubmit={e => registerUser(e)}>

			<h2 className="mt-1 mb-3 text-light">Create a New Account</h2>

			<Form.Group className="mt-3 mb-3" controlId="formBasicName">
	        	<Form.Label className="text-light">Name</Form.Label>
	        	<Form.Control className="w-50" size="sm" type="string" placeholder="Enter full name" value={name} onChange={e => setName(e.target.value)}/>
	      	</Form.Group>

	      	<Form.Group className="mb-3" controlId="formBasicEmail">
	        	<Form.Label className="text-light">Email address</Form.Label>
	        	<Form.Control className="w-50" size="sm" type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	      	</Form.Group>

	      	<Form.Group className="mb-3" controlId="formBasicPassword">
	        	<Form.Label className="text-light">Password</Form.Label>
	        	<Form.Control className="w-50" size="sm" type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}/>
	      	</Form.Group>

	      	<Form.Group className="mb-3" controlId="formBasicPassword2">
	        	<Form.Label className="text-light">Verify Password</Form.Label>
	        	<Form.Control className="w-50" size="sm" type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)}/>
	      	</Form.Group>

	      	{
	      		isActive ?
	      			<Button variant="primary" type="submit" id="submitBtn">
			        Submit
			      	</Button>
			    :
	      			<Button variant="danger" type="submit" id="submitBtn" disabled>
			        Submit
			      	</Button>
	      	}

	      	{/*<Form.Text className="text-muted mt-5">
	        	Already have an account? <Button>Login here</Button>
	        </Form.Text>*/}

	    </Form>
	)
}