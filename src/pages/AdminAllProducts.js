
import ProductCard from "../components/ProductCard";
import { useState, useEffect, useContext, React } from "react";
import UserContext from "../userContext";
import { Navigate } from "react-router-dom";

export default function AllProducts(){

	const [ products, setProducts ] = useState([]);
	const { user } = useContext(UserContext);
	console.log(user);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/viewProducts`)
		.then(response => response.json())
		.then(data => {
			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} productProp={product} />
				)
			}))
		})
	});

	return(
		(!user.isAdmin) ?
            <Navigate to="/products" />
        :
			<>
			<h1 className="text-light text-center">Products</h1>
			<div className="container-fluid">
            	<div>
					{products}
				</div>
        	</div>
			</>
	)
}