
import Banner from "../components/Banner";
import UserContext from "../userContext";
import { useContext } from 'react';

export default function Home(){
    const { user } = useContext(UserContext);
    console.log(user);
    // let [props] = setShowNav;
    // props.funcNav(false);
    let homeData = {};

    let checkUser = function() {
        if(user.id !== null){
            if(user.isAdmin){
                homeData = {
                    title: "Antiques and Collectibles",
                    content: "Welcome, Boss! What would you like to do?",
                    destinationOne: "/adminDashboard",
                    destinationTwo: "/logout",
                    labelOne: "Proceed to Dashboard",
                    labelTwo: "Logout"
                }
            }
            else{
                homeData = {
                    title: "Antiques and Collectibles",
                    content: "Welcome to the site, valued user!",
                    destinationOne: "/products",
                    destinationTwo: "/logout",
                    labelOne: "Browse Products",
                    labelTwo: "Logout"
                }
            }
        }
        else{
            homeData = {
                title: "Antiques and Collectibles",
                content: "One of the most trustworthy online seller, according to an Internet survey. Source: Dude trust me",
                destinationOne: "/login",
                destinationTwo: "/register",
                labelOne: "Login",
                labelTwo: "Register"
            }
        }
        
    };
    checkUser()

    return (
        <>
        <Banner className="container-fluid justify-content-around ml-5 pl-5" data={homeData}/>
        </>
    )
}