
import './App.css';
import AppNavBar from "./components/AppNavBar";
import { UserProvider } from "./userContext";
import { useState, useEffect, React } from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Error from "./pages/Error";
import Home from "./pages/Home";
import AdminDashboard from "./pages/AdminDashboard";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import CreateProduct from "./pages/AdminCreateProduct";
import EditProduct from "./pages/AdminEditProduct";
import AllProducts from "./pages/AdminAllProducts";
import Products from "./pages/Products";
import ProductView from "./pages/ProductView";
import ProductCheckout from "./pages/ProductCheckout";

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    document.body.classList.add('body')
  })

  // const [showNav, setShowNav] = useState(true);
  // funcNav={setShowNav}

  return (
    <>

      <UserProvider value={{user, setUser, unsetUser}}>
      
        <Router>
          <AppNavBar />
          <Container className="container-fluid ml-0 mr-0 p-5">
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/adminDashboard" element={<AdminDashboard />} />
              <Route path="/products" element={<Products />} />
              <Route path="/productView/:productId" element={<ProductView />} />
              <Route path="/productView/:productId/checkout" element={<ProductCheckout />} />
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/createProduct" element={<CreateProduct />} />
              <Route path="/editProduct/:productId" element={<EditProduct />} />
              <Route path="/allProducts" element={<AllProducts />} />
              <Route path="*" element={<Error />} />
            </Routes>
          </Container>
        </Router>
        
      </UserProvider>

    </>
  )
}

export default App;
